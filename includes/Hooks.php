<?php

namespace NonsaCodeJanitor;

use Parser;

class Hooks {

	/**
	 * Runs the transforms.
	 *
	 * @param Parser $parser
	 * @param string $text
	 */
	public static function onParserPreSaveTransformComplete( Parser $parser, string &$text ) {
		$transforms = [
			new Transforms\References(),
			new Transforms\Images()
		];

		foreach ( $transforms as $transform ) {
			$text = $transform->apply( $parser, $text );
		}
	}
}