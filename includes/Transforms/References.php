<?php

namespace NonsaCodeJanitor\Transforms;

use Parser;

class References extends Transform {

	public function apply( Parser $parser, string $wt ) : string {
		if ( !$parser->getTitle()->isContentPage() ) {
			return $wt;
		}

		return preg_replace_callback(
			'/(?:^== *([^=\n]*?) *==\s*)?^<references\s*\/>/sim',
			function ( $m ) {
				if ( empty( $m[1] ) || $m[1] === 'Przypisy' ) {
					return '{{Przypisy}}';
				} else {
					return "{{Przypisy|$m[1]}}";
				}
			},
			$wt
		);
	}
}